package com.epam.rd.java.basic.task7.db;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;



public class DBManager {
    Properties properties = new Properties();
    static Connection connection = getConnection();


    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        return new DBManager();
    }

    public DBManager() {

    }


    public static Connection getConnection() {
        Connection connection = null;
        Properties prop = null;
        try (InputStream input = new FileInputStream("app.properties")) {

            prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            System.out.println(prop.getProperty("connection.url"));

        } catch (IOException ex) {
            ex.printStackTrace();
        }


        try {
            connection = DriverManager.getConnection(prop.getProperty("connection.url"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public List<User> findAllUsers() throws DBException {


        List<User> arrayList = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        ResultSet resultSet=null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM users ");
             resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
                arrayList.add(user);
            }


        } catch (SQLException e) {

            e.printStackTrace();

        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return arrayList;
    }

    public boolean insertUser(User user) throws DBException {
        PreparedStatement preparedStatement1 = null;
        PreparedStatement preparedStatement2 = null;
        try {
            preparedStatement1 = connection.prepareStatement("INSERT INTO users(login) VALUES(?)");

            if (DBManager.IsExist(user.getLogin())) {
                return false;
            }
            preparedStatement1.setString(1, user.getLogin());
            preparedStatement1.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return true;
    }

    public static boolean IsExist(String login) {
        String query = " SELECT login FROM users ";
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        try {
             preparedStatement = connection.prepareStatement(query);
             resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString("login").equals(login)) {
                    return true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;

    }

    public static boolean IsExisteam(String name) {
        String query = " SELECT name FROM teams ";
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        try {
             preparedStatement = connection.prepareStatement(query);
             resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString("name").equals(name)) {
                    return true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;

    }

    public boolean deleteUsers(User... users) throws DBException {
        PreparedStatement preparedStatement1=null;
        for (User user : users) {
            try {
                  preparedStatement1 = connection
                        .prepareStatement("DELETE FROM  users WHERE login=?");
                preparedStatement1.setString(1, user.getLogin());
                preparedStatement1.execute();


            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }finally {
                try {
                    preparedStatement1.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

        return true;
    }

    public User getUser(String login) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = new User();
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM users  WHERE login=?");
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                user.setLogin(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Team tm = new Team();
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM teams  WHERE name=?");
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                tm.setName(resultSet.getString("name"));
                tm.setId(resultSet.getInt("id"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return tm;
    }

    public List<Team> findAllTeams() throws DBException {

        List<Team> arrayList = new ArrayList<>();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("SELECT * FROM teams ");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Team team = new Team();
                team.setName(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                            arrayList.add(team);

            }

            preparedStatement.close();
            resultSet.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    public boolean insertTeam(Team team) throws DBException {
        PreparedStatement preparedStatement1 = null;
        try {
            preparedStatement1 = connection.prepareStatement("INSERT INTO teams(name) VALUES(?)");
            preparedStatement1.setString(1, team.getName());
            System.out.println("Exist &&  " + DBManager.IsExisteam(team.getName()) + team.getName());
            if (DBManager.IsExisteam(team.getName())) {
                System.out.println("існує " + team.getName());
                return false;
            }
            preparedStatement1.execute();


        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                preparedStatement1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
if(user==null||teams==null){
    return  false;
}
        User users = getUser(user.getLogin());
        PreparedStatement preparedStatement=null;


        try {
             preparedStatement = connection.prepareStatement("INSERT INTO users_teams(user_id,team_id) VALUES(?,?) ");
connection.setAutoCommit(false);
            for (Team team :teams) {
            team.setId(getTeam(team.getName()).getId());
            preparedStatement.setInt(1,users.getId());
                System.out.println("kjdj "+user.getId());
            preparedStatement.setInt(2,team.getId());
                System.out.println("sgg "+team.getId());
            preparedStatement.executeUpdate();
            }connection.commit();
            connection.setAutoCommit(true);
          

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }throw new DBException("transaction has been failed",new SQLException());
        }
return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = new ArrayList<>();
        User users = getUser(user.getLogin());
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;

        String querty2 = "SELECT users.login ,teams.id,teams.name FROM users " +
                "INNER JOIN users_teams ON users.id = users_teams.user_id " +
                "INNER JOIN teams ON users_teams.team_id=teams.id  WHERE users.id =?";
        try {
             preparedStatement = connection.prepareStatement(querty2);
            preparedStatement.setInt(1, users.getId());
             resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                System.out.println("я в WHILLEEE");
                System.out.println(resultSet.getString("name"));
                Team team = new Team();
                team.setName(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                list.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    public boolean deleteTeam(Team team) throws DBException {
        System.out.println("ЯЯЯЯЯЯЯ" + team.getId() + team.getName());
        Team team1 = getTeam(team.getName());
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement("DELETE FROM users_teams WHERE team_id=?");
            preparedStatement.setInt(1, team1.getId());
            preparedStatement.execute();


            preparedStatement = connection.prepareStatement("DELETE FROM teams WHERE id=?");
            preparedStatement.setInt(1, team1.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
//		try {
//			preparedStatement=connection.prepareStatement("SELECT  user_id FROM users_teams   WHERE team_id =?");
//			preparedStatement.setInt(1, team1.getId());
//			System.out.println("Team 294 "+team1.getId());
//
//			ResultSet resultSet=preparedStatement.executeQuery();
//while(resultSet.next()){
//	System.out.println("я в while");
//	str.append(resultSet.getInt("user_id"));
//	str.append(",");
//
//}
//for(int i=0;i<str.length();i++){
//	String a=""+str.charAt(i);
//	System.out.println("STR "+str.toString());
//	if(!a.equals(",")){
//		System.out.println("iffff");
//		preparedStatement2=connection.prepareStatement("DELETE   FROM users  WHERE id =?");
//		preparedStatement2.setInt(1,Integer.parseInt(a));
//		preparedStatement2.execute();
//
//	}}
//			preparedStatement1 =connection.prepareStatement("DELETE  FROM  teams WHERE name=?");
//			preparedStatement1.setString(1, team.getName());
//			preparedStatement1.execute();
//			ConnectionUtil.closePreparedStatement(preparedStatement1);
//
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//
//		return true;
//	}

    public boolean updateTeam(Team team) throws DBException {
        System.out.println("UPDATE TEAM 442 "+team.getId());
        try {
            PreparedStatement statement=connection.prepareStatement("UPDATE teams SET name=? WHERE id=?");
            statement.setString(1,team.getName());
            statement.setInt(2,team.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

}
